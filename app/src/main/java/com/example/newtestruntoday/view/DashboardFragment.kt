package com.example.newtestruntoday.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.example.newtestruntoday.databinding.FragmentDashboardBinding
import com.example.newtestruntoday.util.Resource
import com.example.newtestruntoday.view.adapter.MyListAdapter
import com.example.newtestruntoday.viewmodel.ListViewModel
import kotlinx.coroutines.launch

/**
 * Dashboard fragment.
 *
 * @constructor Create empty Dashboard fragment
 */
class DashboardFragment: Fragment() {

    private var _binding: FragmentDashboardBinding? = null
    val binding: FragmentDashboardBinding get() = _binding!!
    private val viewModel by activityViewModels<ListViewModel>()
    private val adapter by lazy { MyListAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentDashboardBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Init views.
     *
     */
    fun initViews(){
        lifecycleScope.launch{
            viewModel.itemList.observe(viewLifecycleOwner, Observer { list ->
                when(list){
                    is Resource.Error -> TODO()
                    Resource.Loading -> TODO()
                    is Resource.Success ->
                        binding.rvView.adapter = adapter.apply { addAnimeList(list.data) }
                }
            })
        }
    }
}

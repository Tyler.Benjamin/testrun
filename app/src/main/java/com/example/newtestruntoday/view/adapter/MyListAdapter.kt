package com.example.newtestruntoday.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.newtestruntoday.databinding.ItemListBinding
import com.example.newtestruntoday.model.entity.AnimeQuote

class MyListAdapter : RecyclerView.Adapter<MyListAdapter.MyListViewHolder>() {

    private var animeList: List<AnimeQuote> = mutableListOf()

    inner class MyListViewHolder(
        val binding: ItemListBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun applyAnimeQuote(animeQuote: AnimeQuote) = with(binding) {
            tvLIST.text = animeQuote.quote
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyListViewHolder {
        val itemBinding =
            ItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyListViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: MyListViewHolder, position: Int) {
        val item = animeList[position]
        holder.applyAnimeQuote(item)
    }

    override fun getItemCount(): Int = animeList.size

    fun addAnimeList(animeList: List<AnimeQuote>) {
        this.animeList = animeList
    }
}

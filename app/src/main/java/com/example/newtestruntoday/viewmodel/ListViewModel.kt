package com.example.newtestruntoday.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.newtestruntoday.data.RepositoryImpl
import com.example.newtestruntoday.model.entity.AnimeQuote
import com.example.newtestruntoday.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * List view model.
 *
 * @property repo
 * @constructor Create empty List view model
 */
@HiltViewModel
class ListViewModel @Inject constructor(private val repo: RepositoryImpl) : ViewModel() {

    private var _itemList: MutableLiveData<Resource<List<AnimeQuote>>> = MutableLiveData()
    val itemList: LiveData<Resource<List<AnimeQuote>>> = _itemList

    init {
        getList()
    }

    /**
     * Get list.
     *
     */
    fun getList() {
        viewModelScope.launch {
            _itemList.value = repo.getList()
        }
    }

}

package com.example.newtestruntoday.repository

import com.example.newtestruntoday.model.entity.AnimeQuote
import com.example.newtestruntoday.util.Resource

/**
 * Repository.
 *
 * @constructor Create empty Repository
 */
interface Repository {
    /**
     * Get list.
     *
     * @return
     */
    suspend fun getList(): Resource<List<AnimeQuote>>
}

package com.example.newtestruntoday.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Testing app.
 *
 * @constructor Create empty Testing app
 */
@HiltAndroidApp
class TestingApp: Application()

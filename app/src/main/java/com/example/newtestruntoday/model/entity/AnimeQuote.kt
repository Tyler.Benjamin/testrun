package com.example.newtestruntoday.model.entity

/**
 * Anime quote data class.
 *
 * @property anime
 * @property character
 * @property quote
 * @constructor Create empty Anime quote
 */
data class AnimeQuote(
    val anime: String,
    val character: String,
    val quote: String
)

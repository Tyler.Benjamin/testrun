package com.example.newtestruntoday.model.remote.dto

import com.example.newtestruntoday.model.entity.AnimeQuote

data class AnimeQuoteDTO(
    val anime: String,
    val character: String,
    val quote: String
)

fun AnimeQuoteDTO.mapper(): AnimeQuote {
    return AnimeQuote(anime = this.anime, quote = this.quote, character = this.character)
}

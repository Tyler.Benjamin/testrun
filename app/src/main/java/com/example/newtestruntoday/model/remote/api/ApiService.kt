package com.example.newtestruntoday.model.remote.api

import com.example.newtestruntoday.model.remote.dto.AnimeQuoteDTO
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET(Companion.LIST_ENDPOINT)
    suspend fun getList(): Response<List<AnimeQuoteDTO>>

    companion object{
        const val BASE_URL ="https://animechan.vercel.app/api/"
        const val LIST_ENDPOINT ="quotes"
    }
}

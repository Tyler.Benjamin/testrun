package com.example.newtestruntoday.data

import com.example.newtestruntoday.model.entity.AnimeQuote
import com.example.newtestruntoday.model.remote.api.ApiService
import com.example.newtestruntoday.model.remote.dto.mapper
import com.example.newtestruntoday.repository.Repository
import com.example.newtestruntoday.util.Resource
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RepositoryImpl @Inject constructor(private val apiService: ApiService): Repository {
    override suspend fun getList(): Resource<List<AnimeQuote>> =
        withContext(Dispatchers.IO){
return@withContext try{
    val res = apiService.getList()
    if(res.isSuccessful&&res.body()!=null){
        Resource.Success(res.body()!!.map { it.mapper() })
    } else {
        Resource.Error("error")
    }
} catch (e:IllegalArgumentException){
    Resource.Error(e.message.toString())
}
        }
}

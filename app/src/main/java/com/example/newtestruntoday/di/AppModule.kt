package com.example.newtestruntoday.di

import com.example.newtestruntoday.data.RepositoryImpl
import com.example.newtestruntoday.model.remote.api.ApiService
import com.example.newtestruntoday.model.remote.api.ApiService.Companion.BASE_URL
import com.example.newtestruntoday.repository.Repository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun providesRetrofitService(): ApiService{
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java);
    }

    @Provides
    @Singleton
    fun proviesRepositroyImpl(apiService: ApiService): Repository = RepositoryImpl(apiService)
}
